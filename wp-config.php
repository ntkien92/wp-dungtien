<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Dev_xemay');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'po /958pJ@/bg^7s0E)(`.hD&dM&bh=K.ywnY.#73z *Q8s>-DYQ;Gx!xhI?|xAk');
define('SECURE_AUTH_KEY',  'OS^Y2fGnSMmgE!jj<11/m^Qft60MRwM$}xTyl8(p~s9psCoM$CBpq3^BS:5qsk[D');
define('LOGGED_IN_KEY',    'n=%TDCrUN]v__NdGW)kZN td8|,Q&ceDb_<@/v4#!{NrK~.4PsbSen#^2(wU+)8E');
define('NONCE_KEY',        'Fo;<n%[dm1AHmm`GG|2,+*d6Q]s|E5kH,arI&4-#!7ho]p6A}Rl#B@(*#_W,gN^[');
define('AUTH_SALT',        'mmVGmn>g=z`f2drV.FVOwoB6(vxaWsQ71jLmE)uXnLX{8u0im$e;xxPaKuTGVUXM');
define('SECURE_AUTH_SALT', '[gOKqVH{Sd6bbu^1)Mkylk;vu:(hM[/K<gg.)Tb_PqK8WTc -{;zc+,1PYxKy7<A');
define('LOGGED_IN_SALT',   '&^>D#Slz85agTtH?#a+MT:ms%{ Nfbt2|&%LYM1r_an3%&K4xY=mGeb?$1{5XZ=&');
define('NONCE_SALT',       'Ri+p/gtWvq;];pp^qH_/,AHy}_(x]qn$X`1-CNxR!N&{Ac3ANZFQ8D+li|8GeiE ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
