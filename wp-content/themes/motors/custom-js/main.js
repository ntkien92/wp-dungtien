// jQuery(document).ready(function($){
  var swiper, mySwiper, mobileSwiper, scroll_top, product_row, wWidth, headerHeight, showsp_pos, outlink_pos, news_pos, footer_pos, resizeTimer, myDraggable;
  var tl = new TimelineLite();
  var tl1 = new TimelineLite();
  var tl2 = new TimelineLite();
  var tl3 = new TimelineLite();
  var tl4 = new TimelineLite();
  var tl5 = new TimelineLite();
  var tl6 = new TimelineLite();
  var tl7 = new TimelineLite();
  var tl_tab = new TimelineLite();
  var wHeight = $(window).height();
  var wWidth = $(window).width();
  var iscrol = true;
  var i = 0;
  var lastScrollTop = 0;

  function Page(){
      var self= this;

      this.init= function() {
          self.fixedMenu();
          self.toggleSubMenuMB();
          self.toggleMenuMB();
          self.searchBox();
          self.toggleFloatingBar();
      self.hideFloatingBar();
      self.popupContact();
          $(window).resize(function() {
              self.toggleFloatingBar();
              self.hideFloatingBar();
          });

      };
    
    this.hideFloatingBar = function(){
      $(window).scroll(function(){
        if($(window).width() <= 1024){
          var threshold = 200; // number of pixels before bottom of page that you want to start fading
          var op = (($(document).height() - $(window).height()) - $(window).scrollTop()) / threshold;
          if( op <= 0 ){
            $("#float-bar-icon").hide();
          } else {
            $("#float-bar-icon").show();
          }
          $("#float-bar-icon").css("opacity", op );
        }
      });
    };
    
    this.popupContact = function(){
      $('.fancyboxContact').fancybox({
        padding: 0,
        margin: 20,
        fitToView: false,
        maxWidth: '100%',
        autoSize: false,
        autoHeight: true,
        closeBtn: false,
      });
    };
    
      this.homepage = function(){

          // self.scrollBar();
          self.calcHeight();
          self.scrollReveal();
          self.mouseDown();
          self.initSwiper();
          self.switchTab();
          self.toggleProductShow();
          self.newsTab();
          self.detectMouseHoverSlide();

          $(window).load(function(){
              showsp_pos = $('#show-sp').offset().top - wHeight + 100;
              outlink_pos = $('#show-sp .center-content').offset().top - wHeight;
              news_pos = $('#news-wrap-mt').offset().top - wHeight + 100;
              self.scrollReveal();
              self.countSlideBanner();
          })

      $(window).scroll(function(){
        showsp_pos = $('#show-sp').offset().top - wHeight + 100;
        outlink_pos = $('#show-sp .center-content').offset().top - wHeight;
        news_pos = $('#news-wrap-mt').offset().top - wHeight + 100;
      });

          $(window).resize(function(event) {
                  self.countSlideBanner();
                  self.switchTab();

              // recalculate height of banner slide
                  self.calcHeight();
                  self.scrollReveal();
                  if (resizeTimer) {
                      clearTimeout(resizeTimer);   // clear any previous pending timer
                  } else {
                  }
                  // set new timer
                  resizeTimer = setTimeout(function() {
                      resizeTimer = null;

                      swiper.update(true);
                      swiper.startAutoplay();
                      self.countSlideBanner();
                  }, 800);

              // sync searchbox status when resize
                  if($('.search-icon').hasClass('active')){
                      $('.searchbox').stop().show();
                  }else if($('.search-icon').hasClass('active')!=1){
                      $('.searchbox').stop().hide();
                  }

              // prevent scroll page after resize when mobile menu is activated
                  if($('#header-mb .menu-icon').hasClass('active')){
                      var wHeight = $(window).height();
                      $('#show').css({
                          'height': wHeight
                      });
                  }

          });

      $('.btn-more-product').each(function(){
        $(this).on('click',function(){
          $(this).parent('.wrap-type').find('.product-item-more').stop().fadeIn(300);
          $(this).hide();
          // $(this).siblings('.remove-product-item').css('display','block');
        });
      });

      // $('.remove-product-item').each(function(){
        // $(this).on('click',function(){
          // $(this).parent('.wrap-type').find('.product-item-more').hide();
          // $(this).hide();
          // $(this).siblings('.btn-more-product').css('display','block');
          // var wrap_top = $(this).parent('.wrap-type').offset().top;
          // $('html,body').animate({scrollTop : wrap_top},1000);
        // });
      // });
      }

    this.sanpham = function(){

          // self.scrollBar();
          $(window).load(function() {
              if(window.location.hash) {
                  var hash = window.location.hash.substring(1);
                  $('.loai-xe-' + hash).trigger('click');
          var top = $('.loai-xe-mb-' + hash).offset().top - 220;
          $('html,body').animate({scrollTop : top},1000);
              }
          });

          self.calcHeight();
          self.scrollReveal();
          self.mouseDown();
          self.initSwiper();
          self.switchTab();
          self.toggleProductShow();
          self.newsTab();
          self.detectMouseHoverSlide();

          $(window).load(function(){
              showsp_pos = $('#show-sp').offset().top - wHeight + 100;
              outlink_pos = $('#show-sp .center-content').offset().top - wHeight;
              // news_pos = $('#news-wrap-mt').offset().top - wHeight + 100;
              self.scrollReveal();
              self.countSlideBanner();
          });
      $(window).scroll(function(){
        showsp_pos = $('#show-sp').offset().top - wHeight + 100;
        outlink_pos = $('#show-sp .center-content').offset().top - wHeight;
      });

      $('.btn-more-product').each(function(){
        $(this).on('click',function(){
          $(this).parent('.wrap-type').find('.product-item-more').stop().fadeIn(300);
          $(this).hide();
          // $(this).siblings('.remove-product-item').css('display','block');
        });
      });

      // $('.remove-product-item').each(function(){
        // $(this).on('click',function(){
          // $(this).parent('.wrap-type').find('.product-item-more').hide();
          // $(this).hide();
          // $(this).siblings('.btn-more-product').css('display','block');
          // var wrap_top = $(this).parent('.wrap-type').offset().top;
          // $('html,body').animate({scrollTop : wrap_top},1000);
        // });
      // });
      }

    this.banggia = function(){

          // self.scrollBar();
          self.calcHeight();
          self.scrollReveal();
          self.mouseDown();
          self.initSwiper();
          self.switchTab();
          self.toggleProductShow();
          self.newsTab();
          self.detectMouseHoverSlide();

          $(window).load(function(){
              showsp_pos = $('#show-sp').offset().top - wHeight + 100;
              outlink_pos = $('#show-sp .center-content').offset().top - wHeight;
              self.scrollReveal();
              self.countSlideBanner();
          })
      $(window).scroll(function(){
        showsp_pos = $('#show-sp').offset().top - wHeight + 100;
        outlink_pos = $('#show-sp .center-content').offset().top - wHeight;
      });
      }
    this.tintuc = function(){

      var swiper = new Swiper('.tab-header', {
      slidesPerView: 'auto',
      paginationClickable: true,
      spaceBetween: 0,
      prevButton : '.swiper-nav .swiper-button-prev',
      nextButton : '.swiper-nav .swiper-button-next',
      onInit: function(){
        if($('.swiper-nav .swiper-button-prev').hasClass('swiper-button-disabled') && $('.swiper-nav .swiper-button-next').hasClass('swiper-button-disabled')){
          $('.swiper-nav').css({
            'display':'none'
          });
          $('.tabs .tab-header .swiper-wrapper').addClass('center-cate');
        }
      }
      });
      setTimeout(function(){
      var toVal = $('.tab-header ul li .active').parent().index();
      swiper.slideTo(toVal,400,false);
      },300);
      }

  // javascrip trang dá»‹ch vá»¥ sau bĂ¡n hĂ ng
      this.service = function(){
          // scroll reveal
          new WOW().init();
          setTimeout(function(){
              $('.wow').removeClass('wow-hidden');
          }, 300);

          $(document).on('click','.block-info .video',function(){
        $('.wrap-slide-album.video-album-pop').fadeIn(300);
        var link = $(this).find('a').data('href');
        location.hash = 'id-youtube=' + link;
        $('.wrap-slide-album.video-album-pop .iframe-wrp').html("<iframe width='100%' height='100%' src='https://www.youtube.com/embed/"+link+"?autoplay=1&showinfo=0&rel=0'></iframe>");
      });

      $('.wrap-slide-album .close-album-hinh-anh').click(function(){
        $('.wrap-slide-album').fadeOut(300);
        $('.wrap-slide-album .iframe-wrp').fadeIn(300);
        $('.wrap-slide-album.video-album-pop .iframe-wrp').html('');
      });
      }

  // javascript trang báº£o hĂ nh - báº£o trĂ¬
      this.maintainance = function(){
          // scroll reveal
          new WOW().init();
          setTimeout(function(){
              $('.wow').removeClass('wow-hidden');
          }, 300)

          // slide dá»‹ch vá»¥ khĂ¡c
          self.owlCarousel();
      }

  // javascript trang danh sĂ¡ch cá»­a hĂ ng
      this.store_list = function(){
          // slide dá»‹ch vá»¥ khĂ¡c
          self.owlCarousel();
      }

      this.toggleFloatingBar = function(){

          wWidth = $(window).width();
          if(wWidth <= 1024){
              if($('#floating-bar').is('hover') !=1){
                  setTimeout(function(){
                      $('#floating-bar').addClass('deactive');
                      $('#float-bar-icon').removeClass('active');
                      $('#float-bar-icon').addClass('inactive');
                  },2000)
              }

              $('#float-bar-icon').click(function(event){
                  event.stopPropagation();
                  $('#floating-bar').stop().toggleClass('deactive');
                  $('#float-bar-icon').stop().toggleClass('active');
                  $('#float-bar-icon').stop().toggleClass('inactive');
              })

              $(window).on('scroll', function(){
                  $('#floating-bar').stop().addClass('deactive');
                  $('#float-bar-icon').stop().removeClass('active');
                  $('#float-bar-icon').stop().addClass('inactive');
              })

              $('body').on('click', function(){
                  $('#floating-bar').stop().addClass('deactive');
                  $('#float-bar-icon').stop().removeClass('active');
                  $('#float-bar-icon').stop().addClass('inactive');
              })
          }else{
              $('#floating-bar').removeClass('deactive');
              $('#float-bar-icon').removeClass('inactive');
              $('#float-bar-icon').addClass('active');
          }
      }

      this.searchBox = function(){
          $('.search').on('click', function(){
              $(this).next('.searchbox').stop().slideToggle();
              if($('.search-icon').hasClass('inactive')){
                  $('.search-icon').stop().removeClass('inactive');
                  $('.search-icon').stop().addClass('active opened');
              } else{
                  $('.search-icon').stop().addClass('inactive');
                  $('.search-icon').stop().removeClass('active opened');
              }
          });
      }

      this.countSlideBanner= function(){
          wWidth = $(window).width();
          if(wWidth > 980){
              var numOfSlide = $("#homepage-slider-mt .swiper-slide").length;
              var barholderW = $("#homepage-slider-mt .swiper-pagination").width();
              $('.swiper-pagination span').css({
                  'width': (barholderW/numOfSlide - 11)
              })
              $(window).on('scroll', function(){
                  $('.swiper-pagination span').css({
                      'width': (barholderW/numOfSlide - 11)
                  })
              })
          }
          if(wWidth <= 980){
              $('.swiper-pagination span').css({
                  'width': 8
              })
              $(window).on('scroll', function(){
                  $('.swiper-pagination span').css({
                      'width': 8
                  })
              })
          }
      };

      this.fixedMenu= function(){
          wWidth = $(window).width();
          if(wWidth > 980 && (navigator.userAgent.match(/iPad/i) == null)){

              $(window).on('scroll', function(){
                  scroll_top = $(window).scrollTop();
          if(scroll_top >= 0){
            if (scroll_top > lastScrollTop){
              if($('#header-pc:hover').length != 1){
                $('body').addClass('menu-hide');
              }
            } else if (scroll_top == 0){
              $("body").removeClass('menu-hide');
            }

            if (scroll_top == 0){
              $('.static-header-pc').stop().animate({height: '117px'},400);
            } else {
              $('.static-header-pc').stop().animate({height: '50px'},100);
            }

            lastScrollTop = scroll_top;
          }
              });

              // $('#header-pc .menu-wrp').hover(function(event){
              //     $("body").removeClass('menu-hide');
              // });

          }
          if(wWidth > 980 && (navigator.userAgent.match(/iPad/i) != null)){

              $(window).on('scroll', function(){
                  scroll_top = $(window).scrollTop();

                  if (scroll_top > lastScrollTop && scroll_top > 117){
                      if($('#header-pc:hover').length != 1){
                          $('body').addClass('menu-hide');
                      }
                  } else if (scroll_top < lastScrollTop){
                      $("body").removeClass('menu-hide');
                  }
                  lastScrollTop = scroll_top;
              });

              // $('#header-pc .menu-wrp').hover(function(event){
              //     $("body").removeClass('menu-hide');
              // });

          }
          if(wWidth <= 980 && wWidth > 700){

              $(window).on('scroll', function(){
                  scroll_top = $(window).scrollTop();

                  if (scroll_top > lastScrollTop && scroll_top > 120){
                      if($('#header-pc:hover').length != 1){
                          $('body').addClass('menu-hide');
                      }
                  } else if (scroll_top < lastScrollTop){
                      $("body").removeClass('menu-hide');
                  }
                  lastScrollTop = scroll_top;
              });
          }
          if(wWidth <= 700){

              $(window).on('scroll', function(){
                  scroll_top = $(window).scrollTop();

                  if (scroll_top > lastScrollTop && scroll_top > 100){
                      if($('#header-pc:hover').length != 1){
                          $('body').addClass('menu-hide');
                      }
                  } else if (scroll_top < lastScrollTop){
                      $("body").removeClass('menu-hide');
                  }
                  lastScrollTop = scroll_top;
              });
          }
      }

      this.scrollBar= function(){
          $(".tabs-content.pc-content").mCustomScrollbar({
              axis:'x',
              advanced:{
                  autoExpandHorizontalScroll: true,
                  updateOnContentResize: true
              },
              mouseWheel:{ enable: false },
              scrollbarPosition: "outside",
              autoHideScrollbar: false,
              autoDraggerLength: true,
              contentTouchScroll: 25,
              documentTouchScroll: true
          });

          if(wWidth > 980){
              var container = $('.tabs-content.all.pc-content');
              var scroller = $('.mCSB_container '); // this holds content after the custom scrollbar plugin is run
              var thumbDragger = $('.mCSB_dragger '); // this is the thumb for dragging the scrollbar

              myDraggable = Draggable.create("#mCSB_1_container", {
                  type:"left",
                  // edgeResistance:0.25,
                  zIndexBoost:false,
                  bounds: container,
                  throwProps:true,
                  onDragStart:function() {
                      // to avoid any conflicts in drag animations
                      container.mCustomScrollbar("stop");
                  },
                  onDragEnd:function() {
                      // edit live css
                      var no = Math.abs(parseInt(scroller.css("left"))) / (parseInt(scroller.css("width")) - parseInt(container.css("width"))) * (parseInt(container.css("width")) - parseInt(thumbDragger.css("width")));
                      thumbDragger.css("left", no);

                      // update the scrollbar (just in case!)
                      container.mCustomScrollbar("update");
                  }
              });
              if($('.show-sp-pc .tabs-content.all').is(":visible")){
                  // myDraggable[0].enable();
              }
              if($('.show-sp-pc .tabs-content.all').is(":visible")!=1){
                  // myDraggable[0].disable();
              }
          };

      };

      this.toggleSubMenuMB= function(){
          $('.list-holder').click(function() {
              $this = $(this);
              if($this.hasClass('active')){
                  $this.find('div').stop().slideUp(300);
                  setTimeout(function(){
                      $this.removeClass('active');
                  },300);
              }else{
                  $('.list-holder.active div').stop().slideUp(300);
                  $this.find('div').stop().slideDown(300);
                  setTimeout(function(){
                      $('.list-holder.active').removeClass('active');
                      $this.addClass('active');
                  },300)
              }
          });
      };

      this.toggleMenuMB= function(){
          $('#header-mb .menu-icon').click(function(){
              if($(this).hasClass('active')){
                  $(this).removeClass('active');
                  $(this).addClass('inactive');
                  $('#show').css({
                      'height': 'auto',
                      'overflow-y': 'auto'
                  });
                  $("#header-mb").removeClass('open');
              } else {
                  $(this).addClass('active');
                  $(this).removeClass('inactive');
                  $('#show').css({
                      'height': wHeight,
                      'overflow-y': 'hidden'
                  });
                  $("#header-mb").addClass('open');
              }
              $('#header-mb .menu-mb').stop().slideToggle(300);
          });
      };

      this.calcHeight= function(){
          if($(window).width() > 980){
               headerHeight = $('#header-pc').innerHeight();
               $('#homepage-slider-mt').css('height', wHeight - headerHeight);
          } else {
               headerHeight = $('#header-mb').innerHeight();
          }
      };

      this.mouseDown= function(){
          TweenMax.to(".mouse-down", 0.8, {bottom: 35, yoyo: true, repeat: -1});
      };

      this.initSwiper= function(){
          if($("#homepage-slider-mt").length > 0){
              swiper = new Swiper('#homepage-slider-mt .swiper-container', {
                  pagination: '.swiper-pagination',
                  autoplay: 5000,
                  speed: 800,
          loop: true,
          autoHeight: false,
          loopedSlides: 0,
                  paginationClickable: true,
                  nextButton: '.next-button',
                  prevButton: '.prev-button',
                  // slidesPerView: "auto",
                  autoplayDisableOnInteraction: false,
                  onInit: function(){
                      if(wWidth > 980){
                          setTimeout(function(){
                              $('.swiper-pagination-bullet-active').addClass('change');
                          },100)
                      }
                      if(wWidth >= 1025){
              setTimeout(function(){
                swiper.stopAutoplay();
              },500);

                      }
                  },
                  onTransitionEnd: function(){
                      if(wWidth > 980){
                          $('.swiper-pagination-bullet-active').addClass('change');
                      }
            if($('.swiper-slide-active .video-slide').length > 0){
              $('.video-slide').each(function(){
                $(this)[0].pause();
                if(wWidth <= 1024){
                  $('.video-controls').show();
                }
              });
              if(wWidth >= 1025){
                $('.swiper-slide-active .video-slide')[0].play();
                $(".swiper-slide-active .video-slide").bind("ended", function() {
                  swiper.slideNext();
                });
              }
            }else{
              setTimeout(function(){
                swiper.slideNext();
              },5000);
            }
                  }
              });
        if(wWidth <= 1024){
          $(window).resize(function(){
            swiper.slideTo(1);
          });
          $('.video-controls').click(function(){
            $(this).siblings('.video-slide')[0].play();
            $(this).hide();
            setTimeout(function(){
              swiper.stopAutoplay();
            },500);
            $(".swiper-slide-active .video-slide").bind("ended", function() {
              swiper.slideNext();
              setTimeout(function(){
                swiper.startAutoplay();
              },500);
            });
          });
          $('.video-slide').click(function(){
            $(this)[0].pause();
            $(this).siblings('.video-controls').show();
            setTimeout(function(){
              swiper.startAutoplay();
            },500);
          });
        }
        $(window).resize(function(){
          if($(window).width() > 980){
             headerHeight = $('#header-pc').innerHeight();
             setTimeout(function(){
              $('#homepage-slider-mt').css('height', $(window).height() - headerHeight);
             },300);
          }
          setTimeout(function(){
            swiper.updateContainerSize();
          },500);

        });
          }

          if($("#bottom-slider").length > 0){
              mySwiper = new Swiper('.bottom-swiper-container', {
                  pagination: '#desktop-swiper-pagination',
                  slidesPerView: 1,
                  autoplay: 5000,
                  centeredSlides: true,
                  speed: 800,
                  paginationClickable: true,
                  autoplayDisableOnInteraction: false,
                  spaceBetween: 25,
                  width: 980,
                  loop : true,
                  nextButton: '.button-next',
                  prevButton: '.button-prev',
                  slidesPerView: "auto"
              });
              mobileSwiper = new Swiper('.mobile-swiper-container', {
                  pagination: '#mobile-swiper-pagination',
                  slidesPerView: 1,
                  autoplay: 5000,
                  centeredSlides: true,
                  paginationClickable: true,
                  autoplayDisableOnInteraction: false,
                  spaceBetween: 25,
                  loop : true,
                  nextButton: '.button-next',
                  prevButton: '.button-prev',
                  slidesPerView: "auto"
              });
          }
      };
      this.detectMouseHoverSlide = function(){
          $('.bottom-swiper-container .button-prev').mouseenter(function(){
              $('.bottom-swiper-container .swiper-slide-prev').addClass('changed');
          }).mouseleave(function(){
              $('.bottom-swiper-container .swiper-slide-prev').removeClass('changed');
          });

          $('.bottom-swiper-container .button-next').mouseenter(function(){
              $('.bottom-swiper-container .swiper-slide-next').addClass('changed');
          }).mouseleave(function(){
              $('.bottom-swiper-container .swiper-slide-next').removeClass('changed');
          });

          $('.bottom-swiper-container .button-prev').click(function(){
              $('.bottom-swiper-container .swiper-slide.changed').removeClass('changed');
              $('.bottom-swiper-container .swiper-slide-prev').addClass('changed');
          });
          $('.bottom-swiper-container .button-next').click(function(){
              $('.bottom-swiper-container .swiper-slide.changed').removeClass('changed');
              $('.bottom-swiper-container .swiper-slide-next').addClass('changed');
          });
      };
      this.switchTab= function(){
      // $('.show-sp-pc .tabs-menu li').click(function(){
        // if($(this).hasClass('current') != 1){
          // $('.show-sp-pc .tabs-menu li.current').stop().removeClass('current');
          // $(this).addClass('current');
          // if($(this).hasClass('mn-allmd')){
            // $('.show-sp-pc .tabs-content .item').stop().fadeIn(0);
          // }
          // if($(this).hasClass('mn-udb')){
            // $('.show-sp-pc .tabs-content .item').stop().fadeOut(0);
            // $('.show-sp-pc .tabs-content .item.udb').stop().fadeIn(0);
          // }
          // if($(this).hasClass('mn-sct')){
            // $('.show-sp-pc .tabs-content .item').stop().fadeOut(0);
            // $('.show-sp-pc .tabs-content .item.sct').stop().fadeIn(0);
          // }
          // if($(this).hasClass('mn-bb')){
            // $('.show-sp-pc .tabs-content .item').stop().fadeOut(0);
            // $('.show-sp-pc .tabs-content .item.bb').stop().fadeIn(0);
          // }
        // }
      // })
      if($('.tabs-menu li').length > 0){
        $('.tabs-menu li').click(function(){
          var menu_id = $(this).attr('id');
          $('.tabs-menu li').removeClass('current');
          $(this).addClass('current');
          if(menu_id == 'loai-xe-all'){
            $('.tabs-content .item').show();
          }else{
            $('.tabs-content .item').hide();
            $('.content-' + menu_id).show();
          }
        });
      }
      };

      this.toggleProductShow= function(){
          $('.show-sp-pc .tabs-content .item').click(function(){
              if($(this).hasClass('active')){
                  TweenMax.to('#show-sp .show-sp-pc .holy .tabs-content .item.active .item-con-big .more',0.1,{delay: 0.3, opacity: 0, bottom:'-40px'});
                  $(this).stop().removeClass('active');
                  self.scrollBar();
              }
              else{
                  TweenMax.to('#show-sp .show-sp-pc .holy .tabs-content .item.active .item-con-big .more',0.1,{delay: 0.3, opacity: 0, bottom:'-40px'});
                  $('.show-sp-pc .tabs-content .item.active').removeClass('active');
                  $(this).addClass('active');
                  TweenMax.to('#show-sp .show-sp-pc .holy .tabs-content .item.active .item-con-big .more',0.3,{delay: 0.3, opacity: 1, bottom:0});
                  self.scrollBar();
              }
          })
      };

      this.newsTab= function(){
          $('.tab-header a').on('click', function(e){
              e.preventDefault();
              var target = $(this).attr('href');
              if(!$(this).hasClass('active')){
                  $('.tab-header a.active').removeClass('active');
                  $(this).addClass('active');
                  tl_tab.to(".tab-content.active", 0.25, {opacity: 0})
                      .to(".tab-content.active", 0, { className : "-=active"})
                      .to(target, 0.25, {opacity: 1},"-=0.25")
                      .to(target, 0, {className: "+=active"});
              }
          });
          $('#tab-select').on('change', function(){
              var target = $(this).val();
              tl_tab.to(".tab-content.active", 0.25, {opacity: 0})
                  .to(".tab-content.active", 0, { className : "-=active"})
                  .to(target, 0.25, {opacity: 1},"-=0.25")
                  .to(target, 0, {className: "+=active"});
          });
      };

    this.threeRound = function(){
          $('.three-round-inner a').on('click', function(e){
              e.preventDefault();
              if(!$(this).hasClass('active') && isAnimating){
                  isAnimating = false;
                  $('.three-round-inner a.active').removeClass('active');
                  var position = parseInt($(this).attr('data-position'));
                  var target = $(this).attr('href');
                  $('.block-quote .active').stop().fadeOut(300);
                  $('.block-quote').removeClass('active');
                  setTimeout(function() {
                      $(target).stop().fadeIn(500, function(){
                          isAnimating = true;
                      });
                      $(target).addClass('active');
                  }, 400);
                  if(position == 2){
                      TweenMax.to("a[data-position=2]", 0.5, {left: 65,  top: 0});
                      TweenMax.to("a[data-position=1]", 0.5, {left: 0, top: "112px"});
                      TweenMax.to("a[data-position=3]", 0.5, {left: 129, top: 112, onComplete: function(){}});
                      setTimeout(function(){
                          $('.three-round-inner a').each(function(){
                              var position_new = parseInt($(this).attr('data-position'));
                              if(position_new == 2) $(this).attr('data-position',1);
                              if(position_new == 3) $(this).attr('data-position',2);
                              if(position_new == 1) $(this).attr('data-position',3);
                          });
                      }, 500);
                  } else if (position == 3){
                      TweenMax.to("a[data-position=3]", 0.5, {left: 65,  top: 0});
                      TweenMax.to("a[data-position=2]", 0.5, {left: 0, top: "112px"});
                      TweenMax.to("a[data-position=1]", 0.5, {left: 129, top: 112, onComplete: function(){}});
                      setTimeout(function(){
                          $('.three-round-inner a').each(function(){
                              var position_new = parseInt($(this).attr('data-position'));
                              if(position_new == 3) $(this).attr('data-position',1);
                              if(position_new == 2) $(this).attr('data-position',3);
                              if(position_new == 1) $(this).attr('data-position',2);
                          });
                      }, 500);
                  }
                  $(this).addClass('active');
                  isAnimating = false;
                  return false;
              }
          });
      };

      this.scrollReveal = function(){
          footer_pos = $('#footer').offset().top;
          scroll_top = $(window).scrollTop();
          wWidth = $(window).width();
          if(scroll_top > showsp_pos){
              tl1.to('#show-sp', 0.8,{opacity: 1, 'padding-top': 0});
              tl1.staggerTo("#show-sp .show-sp-pc .holy .tabs-content .item", 0.6, {opacity: 1}, 0.2);
          }
          if(scroll_top > outlink_pos){
              tl2.staggerTo("#show-sp .link", 0.6, {opacity: 1, 'margin-top': 0}, 0.2);
          }
          if(scroll_top > news_pos){
              tl3.to('#news-wrap-mt', 0.8,{opacity: 1, 'padding-top': 0});
              tl4.staggerTo("#news-wrap-mt  .block", 0.6, {opacity: 1, 'margin-top': 0}, 0.2);
          }
          if(scroll_top > news_pos){
              tl3.to('#news-wrap-mt', 0.8,{opacity: 1, 'padding-top': 0});
              tl4.staggerTo("#news-wrap-mt  .block", 0.6, {opacity: 1, 'margin-top': 0}, 0.2);
          }

          $(window).on('scroll', function(){
              footer_pos = $('#footer').offset().top;
              scroll_top = $(window).scrollTop();
              wWidth = $(window).width();
              if(scroll_top > showsp_pos){
                  tl1.to('#show-sp', 0.8,{opacity: 1, 'padding-top': 0});
                  tl1.staggerTo("#show-sp .show-sp-pc .holy .tabs-content .item", 0.6, {opacity: 1}, 0.2);
              }
              if(scroll_top > outlink_pos){
                  tl2.staggerTo("#show-sp .link", 0.6, {opacity: 1, 'margin-top': 0}, 0.2);
              }
              if(scroll_top > news_pos){
                  tl3.to('#news-wrap-mt', 0.8,{opacity: 1, 'padding-top': 0});
                  tl4.staggerTo("#news-wrap-mt  .block", 0.6, {opacity: 1, 'margin-top': 0}, 0.2);
              }
              if(scroll_top > news_pos){
                  tl3.to('#news-wrap-mt', 0.8,{opacity: 1, 'padding-top': 0});
                  tl4.staggerTo("#news-wrap-mt  .block", 0.6, {opacity: 1, 'margin-top': 0}, 0.2);
              }
          });
      };
      //    Call owl carousel
      this.owlCarousel = function(){
          if($('#owl-carousel').length > 0){
              $("#owl-carousel").owlCarousel({
                  nav:true,
                  dots:false,
                  responsiveClass:true,
                  responsive:{
                      0:{
                          items:2,
                          margin:20,
                      },
                      600:{
                          items:3,
                          margin:20,
                      },
                      800:{
                          items:4,
                          margin:10,
                      },
                      1024:{
                          items:4,
                          margin:20,
                      },
                      1101:{
                          items:4,
                          margin:20,
                      },
                  },
              });
          }
      } ;

    this.phutung = function(){
      if($('.owl-slider').length > 0){
              $(".owl-slider").owlCarousel({
          nav:true,
                  dots:false,
          navRewind: false,
                  responsiveClass:false,
                  items: 3,
          navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        });
      }
    }

    this.phanbiet = function(){
      $('.toggle-sub > li').click(function(){
        if($(this).hasClass('active')){
          $(this).removeClass('active');
          $(this).children('.sub').stop().slideUp(300);
        }else{
          $(this).addClass('active');
          $(this).children('.sub').stop().slideDown(300);
        }
      });
      $('.sub ul li').click(function(e){
        e.stopPropagation();
      });
    }

    this.sanphamchitiet = function(){

      //left nav
      $('.product-detail').sectionScroll({
        topOffset: 50,
      });

      //banner
      self.initSwiper();
      self.calcHeight();
      self.mouseDown();

      //toggle
      $('.op-sub-content .box').each(function(){
        $(this).addClass('index'+$(this).index());
      });

      $('.op-toggle > li > a').click(function(){
        var bg = $(this).attr('data-bg');
        var img = $(this).attr('data-img');
        var img_mb = $(this).attr('data-img-mb');
        var title = $(this).attr('data-title');
        var des = $(this).attr('data-description');
        var color = $(this).attr('data-color');
        
        $('.op-title-bg').css('background-image','url('+bg+')');
        $('.op-title-content .box .image-pc').attr('src',img);
        $('.op-title-content .box .image-mb').attr('src',img_mb);
        $('.block-op .block-left .text .text-title').html(title);
        $('.block-op .block-left .text .des .mCSB_container').html(des);
        $('.op-toggle > li > a').css({
          'color' : color,
          'border' : '1px solid ' + color,
        });
        $('.block-op').css({
          'color' : color,
        });
        
        $('.op-title-bg').removeClass('no-bg');
        
        $('.op-title-content .box').hide();
        $('.op-title-content .box').fadeIn();

        $('.op-title-bg').attr('id',$(this).attr('data-id'));
        
        if(!$(this).hasClass('active')){
          $('.op-toggle > li > a').removeClass('active');
          
          $(this).addClass('active');
          $('.op-option').stop().slideUp().css({'max-height' : '380px', 'opacity' : 0 });
          $(this).siblings('.op-option').slick('unslick');

          $(this).siblings('.op-option').stop().slideDown(400,function(){
            $(this).css({'max-height' : 'none', 'opacity' : 1});
            // setTimeout(function(){
              $(this).slick(getSliderSettings());
            // },300);
          });
          $('.op-option .option-img').removeClass('active');
        }else{
          $('.op-option .option-img').removeClass('active');    
        }
      });

      $('.option-img').click(function(){
        if(!$(this).hasClass('active')){
          var bg = $(this).attr('data-bg');
          var img = $(this).attr('data-img');
          var img_mb = $(this).attr('data-img-mb');
          var title = $(this).attr('data-title');
          var des = $(this).attr('data-description');
          var color = $(this).attr('data-color');

          // $('.op-title-bg').css('background-image','url('+bg+')');
          $('.op-title-content .box .image-pc').attr('src',img);
          $('.op-title-content .box .image-mb').attr('src',img_mb);
          $('.block-op .block-left .text .text-title').html(title);
          // $('.block-op .block-left .text .des').html(des);
          $('.block-op .block-left .text .des .mCSB_container').html(des);
          $('.op-toggle > li > a').css({
            'color' : color,
            'border' : '1px solid ' + color,
          });
          $('.block-op').css({
            'color' : color,
          });

          $('.option .option-img').removeClass('active');
          $('.op-title-bg').addClass('no-bg');
          $(this).addClass('active');

          $('.op-title-content .box').hide();
          $('.op-title-content .box').fadeIn();

          $('.op-title-bg').attr('id','');
        }
      });
      //slick
      $('.op-option').each(function(){
        $(this).slick(getSliderSettings());
      });
      function getSliderSettings(){
        return {
          slidesToShow: 1,
          slidesToScroll: 1,
          touchThreshold: 100,
          autoplay: false,
          dots: true,
          arrows: false,
          infinite: false,
        }
      }
      //mcustombar
      $('.block-op .block-left .text .des').mCustomScrollbar({
        mouseWheelPixels: 200,
      });
      $(window).resize(function(){
        if($(window).width() > 767){
          $('.block-right').mCustomScrollbar({
            mouseWheelPixels: 200,
            scrollbarPosition: "outside"
          });
        }else{
          $('.block-right').mCustomScrollbar({
            live: 'off',
          });
        }
      });
      if($(window).width() > 767){
        $('.block-right').mCustomScrollbar({
          mouseWheelPixels: 200,
          scrollbarPosition: "outside"
        });
      }else{
        $('.block-right').mCustomScrollbar({
          live: 'off',
        });
      }



      //360
      // $('.spritespin').spritespin({
      //  source: SpriteSpin.sourceArray(site_url + '/wp-content/themes/honda/assets/images/img/360/rad_zoom_{frame}.jpg', { frame: [1,34], digits: 3 }),
      //  width: 240,
      //  height: 164,
      //  sense: -1,
      //  behavior: 'drag',
      //  module: '360',
      //  frame: 1,
      //  animate: false,
      // });
      
      var min_height = $('.choose-color').height();
      $('.wrap-color').css('min-height',min_height);

      //fancybox
      $('.fancybox-gallery').fancybox({
        padding: 0,
      });

      //gallery
      $('.wrap-gallery').each(function(){
        $(this).slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          touchThreshold: 100,
          autoplay: false,
          dots: true,
          arrows: false,
          infinite: false,
          nextArrow: '<div class="slick-prev"><i class="fa fa-angle-right"></i></div>',
          prevArrow: '<div class="slick-next"><i class="fa fa-angle-left"></i></div>',
        });
      });

      // timeline
      $('.wrap-history-block').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        touchThreshold: 100,
        asNavFor: '.wrap-timeline',
        autoplay: false,
        dots: false,
        arrows: false,
        infinite: false,
      });
      $('.wrap-timeline').slick({
        autoplay: false,
        arrows: true,
        dots: false,
        asNavFor: '.wrap-history-block',
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: false,
        focusOnSelect: true,
        prevArrow: '<i class="fa fa-arrow-left slick-prev"></i>',
        nextArrow: '<i class="fa fa-arrow-right slick-next"></i>',
        responsive: [
          {
            breakpoint: 1023,
            settings: {
            slidesToShow: 2,
            }
          },
        ],
      });
      //type
      $('.wrap-type').slick({
        autoplay: false,
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        // centerMode: true,
        prevArrow: '<i class="fa fa-chevron-left slick-prev"></i>',
        nextArrow: '<i class="fa fa-chevron-right slick-next"></i>',
        responsive: [
          {
            breakpoint: 1023,
            settings: {
            slidesToShow: 2,
            }
          },
        ],
      });

    }

    this.trietly = function(){
      self.threeRound();
    }

      //Dung Ngo
      this.loadAjaxImg = function(){
          $('.load_ajax_img').each(function(){
              $(this).attr('src', $(this).data('src'));
          });
      };

      this.loadAjaxFloatingBar = function(){
          $.ajax({
              url: ajaxurl,
              method :'POST',
              data: {
                  'action':'floating_bar'
              },
              success:function(data) {
                  $('.floating-bar').append( data );
                  self.toggleFloatingBar();
              },
              error: function(errorThrown){
                  console.log(errorThrown);
              }
          });
      };

      this.loadAjaxData = function(){
          $('.loadAjaxData').each(function(){
              var that = $(this);
              var params = that.data('params');
              $.ajax({
                  url : ajaxurl,
                  method : 'POST',
                  data: {
                      'action' : $(this).data('action'),
                      'params' : params
                  },
                  success:function(data) {
                      that.html(data);
                      window[that.data('callback')]();
                  },
                  error: function(errorThrown){
                      console.log(errorThrown);
                  }
              });
          });
      };
      //End Dung Ngo

      this.fb_share = function(name, link, picture, caption, description) {

          FB.ui( {

              method: 'feed',

              name: name,

              link: link,

              picture: picture,

              description: description,

              caption: caption

              },function(response) {}

          );

      };

    this.update_bootpag = function(){

          var li_last = $('.bootpag li:last');

          if($('.bootpag li.active').next().attr('style') == 'display: none;'){

              li_last.addClass('disabled');

          }else if( li_last.prev().hasClass('active') && li_last.data('lp') < 5 ){

              li_last.addClass('disabled');

          }

      };

    this.show_paging_loading = function(){
          var timePos = Math.floor(Math.random() * 30) + 40;
          var timePosx = timePos + "%";
          var timeDur = timePos * 10;
          $('.pagination-loading-2').fadeIn(20);
          $('.pagination-loading-2').animate({
              width: timePosx
          },timeDur)
      };

    this.hide_paging_loading = function(){
          $('.pagination-loading-2').animate({
              width:'100%'
          },300)
          setTimeout(function(){
              $('.pagination-loading-2').fadeOut(20);
              $('.pagination-loading-2').css('width',0);
          },300)
          setTimeout(function(){
              $("html, body").animate({ scrollTop: 0 }, 'slow');
          },400)
          this.update_bootpag();
      };

  }

  Page = new Page;
  $(document).ready(function(){
      Page.init();

      //Dung Ngo
      setTimeout(function(){
          Page.loadAjaxImg();
          Page.loadAjaxData();
          Page.loadAjaxFloatingBar();
      }, 500);
      //End Dung Ngo
  });

  //Dung Ngo
  //Dá»‹ch vá»¥ khĂ¡c cá»§a honda
  function other_service(){
      Page.owlCarousel();
  }
  //End Dung Ngo
// });