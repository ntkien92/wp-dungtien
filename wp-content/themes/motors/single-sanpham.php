<?php get_header();?>
  <?php get_template_part('partials/page_bg'); ?>
  <?php get_template_part('partials/title_box'); ?>

  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="stm-single-post">
      <div class="detail">
        <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <p><?php the_content(); ?></p>

            <div class="product-detail">
              <?php
                $main_background_images = acf_photo_gallery('backgrounds', $post->ID);

                $main_background = $main_background_images[0]['full_image_url']; //Full size image url
                $main_background_1023_1310 = $main_background_images[1]['full_image_url'];
                $main_background_480_984 = $main_background_images[2]['full_image_url'];

                $design_background_images = acf_photo_gallery('design_backgrounds', $post->ID);
                $design_background = $design_background_images[0]['full_image_url']; //Full size image url
                $design_background_1023_1310 = $design_background_images[1]['full_image_url']; //Full size image url
                $design_background_480_984 = $design_background_images[2]['full_image_url']; //Full size image url
              ?>
              <style type="text/css">
                /* background main */
                .wrap-op{
                  background-image: url(<?php echo $main_background; ?>);
                  background-color: #000;
                }
                /* background theo title */
                .op-title-bg{
                  background-image: url(<?php echo $design_background; ?>);
                  /*background-color: #000;*/
                }
                @media screen and (max-width: 1023px) {
                  .wrap-op{
                    background-image: url(<?php echo $main_background_1023_1310; ?>);
                  }
                  .op-title-bg{
                    background-image: url(<?php echo $design_background_1023_1310; ?>);
                  }
                }
                @media screen and (max-width: 767px) {
                  .wrap-op{
                    background-image: url(<?php echo $main_background_480_984; ?>);
                  }
                  .op-title-bg{
                    background-image: url(<?php echo $design_background_480_984; ?>);
                  }
                }
                .op-toggle > li > a{
                  border: 1px solid #FFFFFF;
                  color: #FFFFFF;
                }
                .block-op{
                  color: #000000;
                }
                .option-text{
                  color: #FFFFFF !important;
                }
              </style>
              <div class="scrollable-section" data-section-title="<i class='menu1'></i><b>Đặc tính nổi bật</b>">
                <!-- background chung -->
                <div class="section-content wrap-op" style="">
                  <!-- background detail -->
                  <div class="op-title-bg">
                    <h2 class="title">Đặc tính nổi bật</h2>
                    <div class="block-op">
                      <div class="block-left">
                        <div class="wrap-op-content">
                          <div class="op-title-content">
                            <div class="box">
                              <div class="img">
                                <img class="image-pc" src="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" alt="" />
                                <img class="image-mb" src="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" alt="" />
                              </div>
                              <div class="text">
                                <div class="text-title"><?php echo $design_background_images[0]['title']; ?></div>
                                <div class="des">
                                  <?php echo $design_background_images[0]['caption']; ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="block-right">
                        <?php
                          $design_images = acf_photo_gallery('designs', $post->ID);
                          $size = 'full'; // (thumbnail, medium, large, full or custom size)
                          if( $design_images ): ?>
                            <ul class="op-toggle">
                              <?php
                                $design_background_images = acf_photo_gallery('design_backgrounds', $post->ID);
                                if ($design_background_images): 
                                  $design_background = $design_background_images[0]['full_image_url']; //Full size image url
                                  $design_background_1023_1310 = $design_background_images[1]['full_image_url'];
                                  $design_background_480_984 = $design_background_images[2]['full_image_url'];
                                  $design_title = $design_background_images[0]['title']; //The title
                                  $design_caption= $design_background_images[0]['caption']; //The caption
                                  ?>
                                  <style type="text/css">
                                    /* background theo title */
                                    #op-title-bg-1 {
                                      background-image: url(<?php echo $design_background ?>) !important;
                                      /*background-color: #000;*/
                                    }
                                    @media screen and (max-width: 1023px) {
                                      #op-title-bg-1{
                                        background-image: url(<?php echo $design_background_1023_1310 ?>) !important;
                                      }
                                    }
                                    @media screen and (max-width: 767px) {
                                      #op-title-bg-1{
                                        background-image: url(<?php echo $design_background_480_984 ?>) !important;
                                      }
                                    }
                                  </style>
                                <?php endif; ?>
                              <li>
                                <a class="active op-title-1" data-id="op-title-bg-1" data-img="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" data-img-mb="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" data-bg="<?php echo $design_background ?>"
                                data-title="<?php echo $design_title ?>" data-caption="<?php echo $design_caption ?>" href="javascript:void(0);" data-color="#FFFFFF">Thiết kế <i class="fa fa-chevron-right"></i></a>
                              </li>
                              <div class="op-option op-option-1" style="display: block" >
                                <div class="option-slide">
                                  <?php foreach( $design_images as $image ): ?>
                                    <?php
                                      $id = $image['id']; // The attachment id of the media
                                      $title = $image['title']; //The title
                                      $caption= $image['caption']; //The caption
                                      $full_image_url= $image['full_image_url']; //Full size image url
                                      $full_image_url = acf_photo_gallery_resize_image($full_image_url, 540, 356); //Resized size to 262px width by 160px height image url
                                      $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                                      $url= $image['url']; //Goto any link when clicked
                                      $target= $image['target']; //Open normal or new tab
                                      $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                                      $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
                                    ?>
                                    <div class="option">
                                      <a href="javascript:void(0);" class="option-img" data-img="<?php echo $full_image_url ?>" data-img-mb="<?php echo $full_image_url ?>" data-bg="" data-title="<?php echo $title ?>" data-caption="<?php echo $caption ?>" data-color="#FFFFFF">
                                        <div class="img" style="background-image: url(<?php echo $full_image_url ?>)"></div>
                                      </a>
                                      <div class="option-text"><?php echo $title ?></div>
                                      <p></p>
                                    </div>
                                  <?php endforeach; ?>
                                  <div class="clearAll"></div>
                                </div>
                              </div>
                              <div class="clearAll"></div>
                            </ul>
                          <?php endif; ?>


                        <?php
                          $engine_and_technology_images = acf_photo_gallery('engine_and_technology', $post->ID);
                          $size = 'full'; // (thumbnail, medium, large, full or custom size)
                          if( $engine_and_technology_images ): ?>
                            <ul class="op-toggle">
                              <?php
                                $engine_and_technology_background_images = acf_photo_gallery('engine_and_technology_backgrounds', $post->ID);
                                if ($design_background_images): 
                                  $engine_and_technology_background = $engine_and_technology_background_images[0]['full_image_url']; //Full size image url
                                  $engine_and_technology_background_1023_1310 = $engine_and_technology_background_images[1]['full_image_url'];
                                  $engine_and_technology_background_480_984 = $engine_and_technology_background_images[2]['full_image_url'];
                                  $engine_and_technology_title = $engine_and_technology_background_images[0]['title']; //The title
                                  $engine_and_technology_caption= $engine_and_technology_background_images[0]['caption']; //The caption
                                  ?>
                                  <style type="text/css">
                                    /* background theo title */
                                    #op-title-bg-2 {
                                      background-image: url(<?php echo $engine_and_technology_background ?>) !important;
                                      /*background-color: #000;*/
                                    }
                                    @media screen and (max-width: 1023px) {
                                      #op-title-bg-2{
                                        background-image: url(<?php echo $engine_and_technology_background_1023_1310 ?>) !important;
                                      }
                                    }
                                    @media screen and (max-width: 767px) {
                                      #op-title-bg-2{
                                        background-image: url(<?php echo $engine_and_technology_background_480_984 ?>) !important;
                                      }
                                    }
                                  </style>
                                <?php endif; ?>
                              <li>
                                <a class="op-title-2" data-id="op-title-bg-2" data-img="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" data-img-mb="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" data-bg="<?php echo $engine_and_technology_background; ?>"
                                data-title="<?php echo $engine_and_technology_title; ?>" data-caption="<?php echo $engine_and_technology_caption; ?>" href="javascript:void(0);" data-color="#FFFFFF">Động cơ & Công nghệ<i class="fa fa-chevron-right"></i></a>
                              </li>
                              <div class="op-option op-option-2" style="display: none" >
                                <div class="option-slide">
                                  <?php foreach( $engine_and_technology_images as $image ): ?>
                                    <?php
                                      $id = $image['id']; // The attachment id of the media
                                      $title = $image['title']; //The title
                                      $caption= $image['caption']; //The caption
                                      $full_image_url= $image['full_image_url']; //Full size image url
                                      $full_image_url = acf_photo_gallery_resize_image($full_image_url, 540, 356); //Resized size to 262px width by 160px height image url
                                      $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                                      $url= $image['url']; //Goto any link when clicked
                                      $target= $image['target']; //Open normal or new tab
                                      $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                                      $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
                                    ?>
                                    <div class="option">
                                      <a href="javascript:void(0);" class="option-img" data-img="<?php echo $full_image_url ?>" data-img-mb="<?php echo $full_image_url ?>" data-bg="" data-title="<?php echo $title ?>" data-caption="<?php echo $title ?>" data-color="#FFFFFF">
                                        <div class="img" style="background-image: url(<?php echo $full_image_url ?>)"></div>
                                      </a>
                                      <div class="option-text"><?php echo $title ?></div>
                                      <p></p>
                                    </div>
                                  <?php endforeach; ?>
                                  <div class="clearAll"></div>
                                </div>
                              </div>
                              <div class="clearAll"></div>
                            </ul>
                          <?php endif; ?>


                          <?php
                            $gadget_images = acf_photo_gallery('gadget', $post->ID);
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)
                            if( $gadget_images ): ?>
                              <ul class="op-toggle">
                                <?php
                                  $gadget_background_images = acf_photo_gallery('gadget_backgrounds', $post->ID);
                                  if ($gadget_background_images): 
                                    $gadget_background = $gadget_background_images[0]['full_image_url']; //Full size image url
                                    $gadget_background_1023_1310 = $gadget_background_images[1]['full_image_url'];
                                    $gadget_background_480_984 = $gadget_background_images[2]['full_image_url'];
                                    $gadget_title = $gadget_background_images[0]['title']; //The title
                                    $gadget_caption= $gadget_background_images[0]['caption']; //The caption
                                    ?>
                                    <style type="text/css">
                                      /* background theo title */
                                      #op-title-bg-3 {
                                        background-image: url(<?php echo $gadget_background ?>) !important;
                                        /*background-color: #000;*/
                                      }
                                      @media screen and (max-width: 1023px) {
                                        #op-title-bg-3 {
                                          background-image: url(<?php echo $gadget_background_1023_1310 ?>) !important;
                                        }
                                      }
                                      @media screen and (max-width: 767px) {
                                        #op-title-bg-3 {
                                          background-image: url(<?php echo $gadget_background_480_984 ?>) !important;
                                        }
                                      }
                                    </style>
                                  <?php endif; ?>

                                <li>
                                  <a class="op-title-3" data-id="op-title-bg-3" data-img="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" data-img-mb="<?php echo get_template_directory_uri(); ?>/assets/images/fake-img.png" data-bg="<?php echo $engine_and_technology_background; ?>"
                                  data-title="<?php echo $gadget_title; ?>" data-cation="<?php echo $gadget_caption; ?>" href="javascript:void(0);" data-color="#FFFFFF">Tiện ích<i class="fa fa-chevron-right"></i></a>
                                </li>
                                <div class="op-option op-option-3" style="display: none" >
                                  <div class="option-slide">
                                    <?php foreach( $gadget_images as $image ): ?>
                                      <?php
                                        $id = $image['id']; // The attachment id of the media
                                        $title = $image['title']; //The title
                                        $caption= $image['caption']; //The caption
                                        $full_image_url= $image['full_image_url']; //Full size image url
                                        $full_image_url = acf_photo_gallery_resize_image($full_image_url, 540, 356); //Resized size to 262px width by 160px height image url
                                        $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                                        $url= $image['url']; //Goto any link when clicked
                                        $target= $image['target']; //Open normal or new tab
                                        $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                                        $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
                                      ?>
                                      <div class="option">
                                        <a href="javascript:void(0);" class="option-img" data-img="<?php echo $full_image_url ?>" data-img-mb="<?php echo $full_image_url ?>" data-bg="" data-title="<?php echo $title ?>" data-caption="<?php echo $caption ?>" data-color="#FFFFFF">
                                          <div class="img" style="background-image: url(<?php echo $full_image_url ?>)"></div>
                                        </a>
                                        <div class="option-text"><?php echo $title ?></div>
                                        <p></p>
                                      </div>
                                    <?php endforeach; ?>
                                    <div class="clearAll"></div>
                                  </div>
                                </div>
                                <div class="clearAll"></div>
                              </ul>
                            <?php endif; ?>
                      </div>
                      <div class="clearAll"></div>
                    </div>
                  </div>
                </div>
              </div>


              <?php
                $specifications = get_field_object('specifications');
                if ($specifications): ?>
                  <div class="scrollable-section active-section specification-section" data-section-title="<i class='menu2'></i><b><?php echo $specifications['label']; ?></b>" id="scrollto-section-3">
                    <div class="section-content">
                      <h2 class="title"><?php echo $specifications['label']; ?></h2>
                      <div class="wrap-spec">
                      <table cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                          <?php foreach ($specifications['value']['body'] as $tr) {
                            echo '<tr>';
                              foreach ($tr as $key=>$td) {
                                if ($key==0) {
                                  echo '<td>';
                                  echo '<b>';
                                  echo $td['c'];
                                  echo '</b>';
                                  echo '</td>';
                                }
                                else {
                                  echo '<td>';
                                    echo $td['c'];
                                  echo '</td>';
                                }
                              }
                            echo '</tr>';
                          } ?>
                        </tbody>
                      </table>
                      </div>
                    </div>
                  </div>  
                <?php endif; ?>


            <?php
              $libraries = acf_photo_gallery('libraries', $post->ID);
              $size = 'full'; // (thumbnail, medium, large, full or custom size)
              if( $libraries ): ?>
                <div class="section-content">
                  <h2 class="title">Thư viện</h2>
                  <?php $library_title = get_field_object('library_title');
                  if ( $library_title ): ?>
                    <div class="news-page">
                      <div class="tabs">
                        <div class="tab-header swiper-container">
                          <ul class="swiper-wrapper center-cate">
                            <li class="swiper-slide">
                              <a class="active" href="#tab-1" data-id="13644" data-title="<?php echo $library_title['value']; ?>"><?php echo $library_title['value']; ?></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  <?php endif ?>

                  <style type="text/css">
                    .slide{
                         position: relative;
                         z-index: 1;
                    }
                    .slider-content{
                         position: absolute;
                         top: 20px;
                         left: 20px;
                         z-index: 100;
                    }
                    .olw-carousel-container {
                      max-width: 940px;
                      margin: auto;
                      padding: 0 30px;
                      min-height: 380px;
                    }
                    .owl-item {
                      width: 33%;
                    }
                    li {
                      list-style-type: none;
                    }

                    .owl-dots .owl-dot span {
                      display: block;
                      width: 8px;
                      height: 8px;
                      background-color: #9d9d9d;
                      border-radius: 50%;
                    }
                  </style>
                  <ul id="owl-demo" class="owl-carousel owl-theme olw-carousel-container">
                    <li>
                      <?php for($idx = 1; $idx <= count($libraries); $idx++): ?>
                        <img class="slide" src="<?php echo $libraries[$idx-1]['full_image_url']; ?>" alt="<?php echo $image['alt']; ?>" onclick="onClick(this)" />
                        <?php if($idx%2 == 0) {
                          echo '</li>';
                          // nếu sau khi đóng, kiểm tra còn items, thì phải mở div mới
                          if($idx < count($libraries)){
                            echo '<li>';
                          }
                        }?>
                      <?php endfor;?>
                      <?php if($idx % 2 != 0) echo '</ul>';?>
                </div>
                <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
                  <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                  <div class="w3-modal-content w3-animate-zoom" style="overflow: visible; width: 702px; height: 461px;">
                    <img id="img01" style="">
                  </div>
                </div>
              <?php endif ?>


            
            <?php
              $colors_1_list = acf_photo_gallery('colors_1_list', $post->ID);
              $colors_1 = acf_photo_gallery('colors_1', $post->ID);
              $colors_2_list = acf_photo_gallery('colors_2_list', $post->ID);
              $colors_2 = acf_photo_gallery('colors_2', $post->ID);
              if($colors_1 && $colors_1_list): ?>
                <div class="scrollable-section section-bang-gia-mau-sac active-section" data-section-title="<i class='menu4'></i><b>Bảng giá &amp; màu sắc</b>" id="scrollto-section-5">
                  <div class="section-content">
                    <h2 class="title">Bảng giá &amp; màu sắc</h2>
                    <div class="wrap-color" style="min-height: 444px;">
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td class="block-360">
                              <div class="">
                                <div class="wrap-360">
                                  <div class="no-360">
                                    <img class="show_price_and_color_img" src="<?php echo $colors_1[0]['full_image_url'] ?>" alt="" />
                                  </div>
                                  <div class="price">
                                    <div class="price-title show_price_and_color_title">
                                      <b><?php echo $colors_1[0]['title'] ?></b>
                                    </div>
                                    <div class="text show_price_and_color_caption">
                                      <?php echo $colors_1[0]['caption'] ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td class="choose-color">
                              <div class="color-box">
                                <div class="box">
                                  <div class="color-title">
                                    <b>
                                      <?php
                                        $colors_1_title = get_field_object('colors_1_title');
                                        echo $colors_1_title['value'];
                                      ?>
                                    </b>
                                  </div>
                                  <div class="color-list">
                                    <?php foreach ($colors_1_list as $key=>$color): ?>
                                      <?php
                                        $title = $color['title']; //The title
                                        $caption= $color['caption']; //The caption
                                        $full_image_url= $color['full_image_url']; //Full size image url
                                      ?>
                                      <a href="javascript:void(0);" class="item get_data_version price_and_color" data-img="<?php echo $colors_1[$key]['full_image_url']; ?>" data-title="<?php echo $colors_1[$key]['title']; ?>" data-caption="<?php echo $colors_1[$key]['caption']; ?>">
                                        <div class="text"><?php echo $title ?></div>
                                        <div class="img">
                                          <img src="<?php echo $full_image_url ?>" alt="" />
                                        </div>
                                        <div class="clearAll"></div>
                                        <div class="data_version" style="display:none">
                                          <div class="no-360">
                                            <img src="<?php echo $colors_1[$key]['full_image_url'] ?>" alt="" />
                                          </div>
                                          <div class="price">
                                            <div class="price-title">Giá bán lẻ đề xuất</div>
                                            <div class="text">37.490.000 VNĐ</div>
                                          </div>
                                        </div>
                                      </a>
                                    <?php endforeach ?>
                                  </div>
                                  <div class="clearAll"></div>
                                </div>
                                <?php if($colors_2 && $colors_2_list): ?>
                                  <div class="box">
                                    <div class="color-title">
                                      <b>
                                        <?php
                                          $colors_2_title = get_field_object('colors_2_title');
                                          echo $colors_2_title['value'];
                                        ?>
                                      </b>
                                    </div>
                                    <div class="color-list">
                                      <?php foreach ($colors_2_list as $key=>$color): ?>
                                        <?php
                                          $title = $color['title']; //The title
                                          $caption= $color['caption']; //The caption
                                          $full_image_url= $color['full_image_url']; //Full size image url
                                        ?>
                                        <a href="javascript:void(0);" class="item get_data_version price_and_color" data-img="<?php echo $colors_2[$key]['full_image_url']; ?>" data-title="<?php echo $colors_2[$key]['title']; ?>" data-caption="<?php echo $colors_2[$key]['caption']; ?>">
                                          <div class="text"><?php echo $title ?></div>
                                          <div class="img">
                                            <img src="<?php echo $full_image_url ?>" alt="" />
                                          </div>
                                          <div class="clearAll"></div>
                                          <div class="data_version" style="display:none">
                                            <div class="no-360">
                                              <img src="<?php echo $colors_2[$key]['full_image_url'] ?>" alt="" />
                                            </div>
                                            <div class="price">
                                              <div class="price-title">Giá bán lẻ đề xuất</div>
                                              <div class="text">37.490.000 VNĐ</div>
                                            </div>
                                          </div>
                                        </a>
                                      <?php endforeach; ?>
                                    </div>
                                    <div class="clearAll"></div>
                                  </div>
                                <?php endif; ?>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              <?php endif; ?>

              <?php 
                $versions = get_field_object('versions', $post->ID);

                if ($versions):
                  $version_title = $versions['title'];
                  $version_images = acf_photo_gallery('versions', $post->ID); ?>
                  <style type="text/css">
                    .product-detail .wrap-timeline:before {
                      content: '';
                      position: absolute;
                      top: 90px;
                      left: 50%;
                      transform: translate(-50%, 0);
                      -webkit-transform: translate(-50%, 0);
                      -moz-transform: translate(-50%, 0);
                      -o-transform: translate(-50%, 0);
                      width: calc(100% - 178px);
                      border-top: 1px dashed #666;
                    }
                  </style>
                  <div class="scrollable-section active-section" data-section-title="<i class='menu6'></i><b>Các đời xe</b>" id="scrollto-section-6">
                    <div class="section-content">
                      <h2 class="title">Các đời xe</h2>
                      <div class="wrap-history">
                        <div class="wrap-history-block slick-initialized slick-slider">
                          <div aria-live="polite" class="slick-list draggable">
                            <div class="slick-track">
                              <div class="history-block slick-slide" style="width: 1180px;">
                                <div class="history-slide">
                                  <div class="img "><img class="history-slide-img" src="<?php echo $version_images[0]['full_image_url']; ?>" alt="" />
                                  </div>
                                  <div class="text">
                                    <p><b><?php echo $version_images[0]['title']; ?></b></p>
                                    <p><?php echo $version_images[0]['caption']; ?></p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-timeline slick-initialized slick-slider">  
                        <div aria-live="polite" class="slick-list draggable">
                          <div class="slick-track" style="opacity: 1; width: 440px; transform: translate3d(0px, 0px, 0px);" role="listbox">
                            <?php foreach ($version_images as $key=>$version_image):
                              $title = $version_image['title']; //The title
                              $full_image_url= $version_image['full_image_url']; //Full size image url
                            ?>
                              <div class="timeline slick-slide slick-active time_version_<?php echo $key?>" style="width: 220px;" data-img="<?php echo $full_image_url; ?>">
                                <div class="img">
                                  <img src="<?php echo $full_image_url; ?>" alt="" />
                                </div>
                                <div class="dots"></div>
                                <div class="year"><?php echo $title; ?></div>
                                <div class="timeline-title"></div>
                              </div>
                              <script type="text/javascript">
                                jQuery(function($){
                                  $(document).ready(function(){
                                    var key = <?php echo $key ?>;
                                    if (key == 0) {
                                      $('.time_version_0').addClass('slick-current');
                                    };
                                  });
                                });
                              </script>
                            <?php endforeach; ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php get_footer();?>

<script type="text/javascript">
  $(document).ready(function() {
    var templateUrl = '<?= get_bloginfo("template_url"); ?>';

    $('.op-title-1').click(function() {
      $(this).addClass("active");


      $('.image-pc').attr('src', templateUrl + '/assets/images/fake-img.png');
      $('.image-mb').attr('src', templateUrl + '/assets/images/fake-img.png');

      $('.op-title-2').removeClass("active");
      $('.op-title-3').removeClass("active");

      $('.op-option-1').slideToggle("slow", function(){});
      $('.op-option-2').hide("slideDown");
      $('.op-option-3').hide("slideDown");

      $('.op-title-bg').attr('id', 'op-title-bg-1');

      $('.text-title').text($(this).data('title'));
      $('.des').text($(this).data('caption'));
    });

    $('.op-title-2').click(function() {
      $(this).addClass("active");


      $('.image-pc').attr('src', templateUrl + '/assets/images/fake-img.png');
      $('.image-mb').attr('src', templateUrl + '/assets/images/fake-img.png');

      $('.op-title-1').removeClass("active");
      $('.op-title-3').removeClass("active");

      $('.op-option-1').hide("slideDown");
      $('.op-option-2').slideToggle("slow", function(){});
      $('.op-option-3').hide("slideDown");

      $('.op-title-bg').attr('id', 'op-title-bg-2');

      $('.text-title').text($(this).data('title'));
      $('.des').text($(this).data('caption'));
    });

    $('.op-title-3').click(function() {
      $(this).addClass("active");


      $('.image-pc').attr('src', templateUrl + '/assets/images/fake-img.png');
      $('.image-mb').attr('src', templateUrl + '/assets/images/fake-img.png');

      $('.op-title-1').removeClass("active");
      $('.op-title-2').removeClass("active");

      $('.op-option-1').hide("slideDown");
      $('.op-option-2').hide("slideDown");
      $('.op-option-3').slideToggle("slow", function(){});

      $('.op-title-bg').attr('id', 'op-title-bg-3');

      $('.text-title').text($(this).data('title'));
      $('.des').text($(this).data('caption'));
    });

    $('.option-img').click(function() {
      $('.op-title-bg').addClass('no-bg');
      $('.op-title-bg').removeAttr('id');

      var img = $(this).data('img');
      // $('.image-pc').attr('src', $(this).data('img'));
      // $('.image-mb').attr('src', $(this).data('img'));

      $('.image-pc').fadeOut(100, function(){
        $(this).attr('src', img);
        $(this).fadeIn(100);
      });

      $('.text-title').text($(this).data('title'));
      $('.des').text($(this).data('caption'));
      console.log($(this).data('caption'));
    });
  });

  //libraries

  function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
  };

  // Bảng giá và màu sắc
  $('.price_and_color').click(function() {
    var img = $(this).data('img');
    var title = $(this).data('title');
    var caption = $(this).data('caption');

    $('.show_price_and_color_img').fadeOut(300, function(){
      $('.show_price_and_color_img').attr('src', img);
      $('.show_price_and_color_title').text(title);
      $('.show_price_and_color_caption').text(caption);
      $(this).fadeIn(300);
    });
  });

  // Timeline
  $('.timeline').click(function() {
    $('.timeline').removeClass('slick-current');
    $(this).addClass('slick-current');

    var img = $(this).data('img');
    $('.history-slide-img').fadeOut(300, function(){
      $(this).attr('src', img);
      $(this).fadeIn(300);
    });
  });
</script>

<script>
  jQuery(function($){
      $(document).ready(function(){
          var owl = $('.owl-carousel');
              owl.owlCarousel({
                  items:1,
                  margin:10,
                  autoplayHoverPause:true,
                  navText: ["<i class='fa fa-chevron-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right' aria-hidden='true'></i>"],
                  navClass: ['owl-prev', 'owl-next'],
                  nav: true,
                  responsive : {
                    480 : { items : 1, nav: true  }, // from zero to 480 screen width 4 items
                    768 : { items : 2, nav: true  }, // from 480 screen widthto 768 6 items
                    1024 : { items : 3, nav: false    // from 768 screen width to 1024 8 items
                  }
              },
          });
          $('.play').on('click',function(){
              owl.trigger('play.owl.autoplay',[1000])
          })
          $('.stop').on('click',function(){
              owl.trigger('stop.owl.autoplay')
          })
      });
  })
</script>

