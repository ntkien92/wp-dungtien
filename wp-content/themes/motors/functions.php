<?php

    if(is_admin()) {
        require_once get_template_directory() . '/admin/admin.php';
    }

	define( 'STM_TEMPLATE_URI', get_template_directory_uri() );
	define( 'STM_TEMPLATE_DIR', get_template_directory() );
	define( 'STM_THEME_SLUG', 'stm' );
	define( 'STM_INC_PATH', get_template_directory() . '/inc' );
	define( 'STM_CUSTOMIZER_PATH', get_template_directory() . '/inc/customizer' );
	define( 'STM_CUSTOMIZER_URI', get_template_directory_uri() . '/inc/customizer' );

	//	Include path
	$inc_path = get_template_directory() . '/inc';

	//	Widgets path
	$widgets_path = get_template_directory() . '/inc/widgets';


	define('motors', 'motors');

		// Theme setups
		require_once STM_CUSTOMIZER_PATH . '/customizer.class.php';

		// Custom code and theme main setups
		require_once( $inc_path . '/setup.php' );

		// Enqueue scripts and styles for theme
		require_once( $inc_path . '/scripts_styles.php' );

        // Multiple Currency
        require_once( $inc_path . '/multiple_currencies.php' );

		// Custom code for any outputs modifying
		require_once( $inc_path . '/custom.php' );

		// Required plugins for theme
		require_once( $inc_path . '/tgm/tgm-plugin-registration.php' );

		// Visual composer custom modules
		if ( defined( 'WPB_VC_VERSION' ) ) {
			require_once( $inc_path . '/visual_composer.php' );
		}

		// Custom code for any outputs modifying with ajax relation
		require_once( $inc_path . '/stm-ajax.php' );

		// Custom code for filter output
		//require_once( $inc_path . '/listing-filter.php' );
		require_once( $inc_path . '/user-filter.php' );

		//User
		if(stm_is_listing()) {
			require_once( $inc_path . '/user-extra.php' );
		}

		require_once( $inc_path . '/user-vc-register.php' );

		require_once( $inc_path . '/stm_single_dealer.php' );

		// Custom code for woocommerce modifying
		if( class_exists( 'WooCommerce' ) ) {
		    require_once( $inc_path . '/woocommerce_setups.php' );
            if(stm_is_rental()) {
                require_once( $inc_path . '/woocommerce_setups_rental.php' );
            }
		}

		//Widgets
		require_once( $widgets_path . '/socials.php' );
		require_once( $widgets_path . '/text-widget.php' );
		require_once( $widgets_path . '/latest-posts.php' );
		require_once( $widgets_path . '/address.php' );
		require_once( $widgets_path . '/dealer_info.php' );
        require_once( $widgets_path . '/car_location.php' );
		require_once( $widgets_path . '/similar_cars.php' );
		require_once( $widgets_path . '/car-contact-form.php' );
		require_once( $widgets_path . '/contacts.php' );
		if(stm_is_boats()) {
			require_once( $widgets_path . '/schedule_showing.php' );
			require_once( $widgets_path . '/car_calculator.php' );
		}

  # Custom post
  function create_custom_post_type() {

    $label = array('name' => 'Các sản phẩm', 
                    'singular_name' => 'Sản phẩm', 
                    'add_new' => 'Thêm sản phẩm', 
                    'add_new_item' => 'Thêm sản phẩm mới', 
                    'edit_item' => 'Sửa sản phẩm', 
                    'new_item' => 'Sản phẩm mới', 
                    'view_item' => 'Xem sản phẩm', 
                    'view_items'=>'Xem tất cả sản phẩm', 
                    'search_items'=>'Tìm kiếm sản phẩm', 
                    'not_found'=>'Không có sản phẩm', 
                    'not_found_in_trash'=>'Không có sản phẩm nào trong thùng rác', 
                    'parent_item_colon'=>'Danh mục cha', 
                    'all_items'=>'Tất cả sản phẩm', 
                    'archives'=>'Danh mục', 
                    'attributes'=>'Thuộc tính', 
                    'insert_into_item'=>'Thêm phương tiện', 
                    'uploaded_to_this_item'=>'Tải lên phương tiện', 
                    'featured_image'=>'Ảnh sản phẩm', 
                    'set_featured_image'=>'Thêm hình ảnh sản phẩm', 
                    'remove_featured_image'=>'Xóa hình ảnh sản phẩm', 
                    'use_featured_image'=>'Sử dụng hình ảnh sản phẩm', 
                    'menu_name'=>'Sản phẩm', 
                    'name_admin_bar'=>'Sản phẩm',);

    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */

    $args = array(
      //Tham số cấu hình cho custom post type
      'labels' => $label, //Gọi các label trong biến $label ở trên
      'description' => 'Post type đăng sản phẩm', //Mô tả của post type
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'author',
        'thumbnail',
        'comments',
        'trackbacks',
        'revisions',
        'custom-fields'
      ), //Các tính năng được hỗ trợ trong post type
      'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
      'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
      'public' => true, //Kích hoạt post type
      'show_ui' => true, //Hiển thị khung quản trị như Post/Page
      'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
      'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
      'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
      'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
      'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
      'can_export' => true, //Có thể export nội dung bằng Tools -> Export
      'has_archive' => true, //Cho phép lưu trữ (month, date, year)
      'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
      'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
      'capability_type' => 'post' //
    );

    register_post_type( 'sanpham' , $args ); //Cái slug-post-type rất quan trọng, bạn có thể đặt tùy ý nhưng không có dấu cách, ký tự,...

  }
  add_action( 'init', 'create_custom_post_type' );
  flush_rewrite_rules( false );

  /**
  @ Chèn CSS và Javascript vào theme
  @ sử dụng hook wp_enqueue_scripts() để hiển thị nó ra ngoài front-end
  **/
  function hoang_styles() {
    /*
     * Hàm get_stylesheet_uri() sẽ trả về giá trị dẫn đến file style.css của theme
     * Nếu sử dụng child theme, thì file style.css này vẫn load ra từ theme mẹ
     */
    wp_register_style('home-style', get_template_directory_uri() . '/custom-css/home.css', '1.0', 'all');
    wp_enqueue_style('home-style');

    wp_register_style('custom-style', get_template_directory_uri() . '/custom-css/custom.css', '1.0', 'all');
    wp_enqueue_style('custom-style');

    wp_register_style('san-pham-style', get_template_directory_uri() . '/custom-css/san-pham.css', '1.0', 'all');
    wp_enqueue_style('san-pham-style');

    wp_register_style('tin-tuc-style', get_template_directory_uri() . '/custom-css/tin-tuc.css', '1.0', 'all');
    wp_enqueue_style('tin-tuc-style');

    wp_register_style('w3-style', get_template_directory_uri() . '/custom-css/w3.css', '1.0', 'all');
    wp_enqueue_style('w3-style');

    wp_register_style('h3-style', get_template_directory_uri() . '/custom-css/h3.css', '1.0', 'all');
    wp_enqueue_style('h3-style');
  };
  add_action( 'wp_enqueue_scripts', 'hoang_styles' );

  function hoang_scripts() {

    // wp_register_script('jquery-1.11.3-script', get_template_directory_uri() . '/custom-js/jquery-1.11.3.js');
    // wp_enqueue_script('jquery-1.11.3-script');

    // wp_register_script('TweenMax-script', get_template_directory_uri() . '/custom-js/tween/TweenMax.js', array( 'jquery' ), '1.0', true);
    // wp_enqueue_script('TweenMax-script');

    // wp_register_script('Draggable-script', get_template_directory_uri() . '/custom-js/tween/utils/Draggable.js', array( 'jquery' ), '1.0', true);
    // wp_enqueue_script('Draggable-script');

    // wp_register_script('main-script', get_template_directory_uri() . '/custom-js/main.js', array( 'jquery' ), '1.0', true);
    // wp_enqueue_script('main-script');

    // wp_register_script('jquery.section-scroll-script', get_template_directory_uri() . '/custom-js/jquery.section-scroll.js', array( 'jquery' ), '1.0', true);
    // wp_enqueue_script('jquery.section-scroll-script');

    // wp_register_script('slick-script', get_template_directory_uri() . '/custom-js/slick.min.js', array( 'jquery' ), '1.0', true);
    // wp_enqueue_script('slick-script');
    wp_register_script('h3', get_template_directory_uri() . '/custom-js/h3.js', array( 'jquery' ), 'all');
    wp_enqueue_script('h3');
  };
  add_action( 'wp_enqueue_scripts', 'hoang_scripts' );

